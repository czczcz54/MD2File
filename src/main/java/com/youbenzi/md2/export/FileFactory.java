package com.youbenzi.md2.export;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;

import com.youbenzi.md2.markdown.Block;
import com.youbenzi.md2.markdown.MDAnalyzer;

/**
 * 文档生成工厂
 *
 * @author yangyingqiang
 * 2015年5月1日 下午2:35:08
 */
public class FileFactory {

    public static String Encoding = "UTF-8";

    public static void produce(File file, String outputFilePath) throws FileNotFoundException, UnsupportedEncodingException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), Encoding));

        produce(reader, outputFilePath);
    }

    public static void produce(InputStream is, String outputFilePath) throws FileNotFoundException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName(Encoding)));

        produce(reader, outputFilePath);
    }

    public static void produce(String mdText, String outputFilePath) throws FileNotFoundException {
        BufferedReader reader = new BufferedReader(new StringReader(mdText));

        produce(reader, outputFilePath);
    }

    public static void produce(BufferedReader reader, String outputFilePath) throws FileNotFoundException {
        List<Block> list = MDAnalyzer.analyze(reader);
        produce(list, outputFilePath);
    }

    public static void produce(List<Block> list, String outputFilePath) throws FileNotFoundException {
        String ext = getExtOfFile(outputFilePath);
        Decorator decorator = BuilderFactory.build(ext);

        decorator.beginWork(outputFilePath);
        decorator.decorate(list);
        decorator.afterWork(outputFilePath);
    }

    private static String getExtOfFile(String outputFilePath) {
        if (outputFilePath == null) {
            return "";
        }
        int i = outputFilePath.lastIndexOf(".");
        if (i < 0) {
            return "";
        }
        return outputFilePath.substring(i + 1);
    }

    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {

        FileFactory.produce(new File("/Users/cevin/Downloads/测试.md"), "/Users/cevin/Downloads/simple.docx");
    }
}
