package com.yubenzi.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import com.youbenzi.md2.export.PDFDecorator5x;
import org.junit.Ignore;
import org.junit.Test;

import com.youbenzi.md2.export.FileFactory;
import com.youbenzi.md2.util.MDUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProduceTest {
    private Logger logger = LoggerFactory.getLogger(ProduceTest.class);

    @Ignore
    @Test
    public void testExport(){
        String filePath = "D:\\install_office\\Ideal\\study_workspace\\MD2File\\src\\test\\resources\\md_for_test.md";
//        String filePath = "C:\\Users\\zhengchen5\\Desktop\\1.md";

        try {
            // 导出文本
            FileFactory.produce(new File(filePath), "C:\\Users\\zhengchen5\\Desktop\\test.docx");
            FileFactory.produce(new File(filePath), "C:\\Users\\zhengchen5\\Desktop\\test.pdf");
//            FileFactory.produce(new File(filePath), "C:\\Users\\zhengchen5\\Desktop\\test.html");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

	@Ignore
    @Test
    public void test() {
        File file = new File(ProduceTest.class.getResource("/md_for_test.md").getFile());
        try {
            String[] strs = {"pdf","html","docx"};
            for (String s : strs) {
                File newFile = new File(file.getParentFile().getParentFile(), "tmp/test." + s);
                FileFactory.produce(file, newFile.getAbsolutePath());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
}