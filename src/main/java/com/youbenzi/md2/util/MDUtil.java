package com.youbenzi.md2.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import com.youbenzi.md2.export.BuilderFactory;
import com.youbenzi.md2.export.HTMLDecorator;
import com.youbenzi.md2.markdown.Block;
import com.youbenzi.md2.markdown.MDAnalyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MDUtil {

	private static Logger logger = LoggerFactory.getLogger(MDUtil.class);

	public static String markdown2Html(File file){
		BufferedReader reader = null;
		String lineStr = null;
		try {
			reader = new BufferedReader(new FileReader(file));StringBuffer sb = new StringBuffer();
			while ((lineStr = reader.readLine())!=null) {
				sb.append(lineStr).append("\n");
			}
			return markdown2Html(sb.toString());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if(reader!=null){
				try {
					reader.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return null;
	}
	
	public static String markdown2Html(String mdStr){
		if(mdStr==null){
			return null;
		}
		BufferedReader reader = new BufferedReader(new StringReader(mdStr));
		List<Block> list = MDAnalyzer.analyze(reader);
		
		HTMLDecorator decorator = new HTMLDecorator(); 
		
		decorator.decorate(list);
		
		return decorator.outputHtml();
	}
	
}
