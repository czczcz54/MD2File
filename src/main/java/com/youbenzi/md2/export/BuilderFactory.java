package com.youbenzi.md2.export;

import com.youbenzi.md2.export.builder.DecoratorBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 文档装饰器的工厂，使用反射来生成相应装饰器。使用反射的原因是为了降低MD2File的包耦合度
 * @author yangyingqiang
 * 2015年5月15日 下午10:00:39
 */
public class BuilderFactory {

	private static Logger logger = LoggerFactory.getLogger(BuilderFactory.class);

	private static DecoratorBuilder initDecoratorBuilder(String className){
		try {
			@SuppressWarnings("rawtypes")
			Class clazz = Class.forName(className);
			return (DecoratorBuilder)clazz.newInstance();
		} catch (InstantiationException e) {
			logger.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	private static final String DOC_BUILDER_CLASS_NAME = "com.youbenzi.md2.export.builder.DocDecoratorBuilder";
	private static final String DOCX_BUILDER_CLASS_NAME = "com.youbenzi.md2.export.builder.DocxDecoratorBuilder";
	private static final String PDF_5X_BUILDER_CLASS_NAME = "com.youbenzi.md2.export.builder.PDFDecoratorBuilder5x";
	private static final String HTML_BUILDER_CLASS_NAME = "com.youbenzi.md2.export.builder.HTMLDecoratorBuilder";
	
	public static Decorator build(String ext) {
		DecoratorBuilder decoratorBuilder;
		if("docx".equalsIgnoreCase(ext)){
			decoratorBuilder = initDecoratorBuilder(DOCX_BUILDER_CLASS_NAME);
		}else if("doc".equalsIgnoreCase(ext)){
			decoratorBuilder = initDecoratorBuilder(DOC_BUILDER_CLASS_NAME);
		}else if("pdf".equalsIgnoreCase(ext)){
			decoratorBuilder = initDecoratorBuilder(PDF_5X_BUILDER_CLASS_NAME);
		}else if("html".equalsIgnoreCase(ext) || "htm".equalsIgnoreCase(ext)){
			decoratorBuilder = initDecoratorBuilder(HTML_BUILDER_CLASS_NAME);
		}else{
			throw new RuntimeException("请确认输出的文档为docx，doc，pdf，html的文档格式");
		}
		Decorator decorator = decoratorBuilder.build();
		return decorator;
	}

}
